// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
      apiKey: "AIzaSyCz7EK1KHWC98JHtgwKADF1zGASzgo1ecU",
      authDomain: "app2ircl.firebaseapp.com",
      databaseURL: "https://app2ircl.firebaseio.com",
      projectId: "app2ircl",
      storageBucket: "app2ircl.appspot.com",
      messagingSenderId: "830645677277",
      appId: "1:830645677277:web:44358ba503c9ad63853f34"
    }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
