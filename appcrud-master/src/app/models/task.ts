export interface Task {
    id?:string;
    task: string;
    priority: number;
    owner: string;
    place: string;
}
